﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace monarchui
{
    public enum CommandTypes
    {
        ETFDETAILS,
        ETFDISABLE,
        ENABLE,
        RESET_ACTIVITY,
        ALGOCHANGE,
        ALGOCONFIG,
        ALGOSAVE,
        ETFSAVE,
        ETFCONFIG,
        CLIENTCONNECTED,
        UPDATEPOSITION,
        CANCELALL,
        DISABLEALL,
        HEDGEETF,
        HEDGECOMPLONG,
        HEDGECOMPSHORT,
        HEDGECOMPLONGMM,
        HEDGECOMPSHORTMM,
        ABOUT,
        RECONCILE_NAVS,
        OFFERLIQUIDITY,
        DISABLELIQUIDITY,
        ACTIVATELIQUIDITY,
        ATTACK,
        RETREAT,
        HITLIFT,
        EXTERNALFILL
    }
}
