﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ServiceStack.Text;

namespace monarchui
{
    public class Configuration
    {
        public string SubscribeEndpoint { get; set; }
        public string ReqRepEndpoint { get; set; }




        private static volatile Configuration instance;
        private static object syncRoot = new Object();

        private Configuration() { }

        public static Configuration Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new Configuration();


                            try
                            {
                                instance = JsonSerializer.DeserializeFromString<monarchui.Configuration>(File.ReadAllText(@"conf/zeromq.json"));
                            }
                            catch (Exception ert)
                            {
                                Console.WriteLine("ERR: " + ert.Message);
                            }

                        }
                    }
                }

                return instance;
            }

        }
    }
}
