﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using NetMQ;
using ServiceStack.Text;

namespace monarchui
{
    public class SignalEventArgs : EventArgs
    {
        public Signal Sig { get; set; }
    }

    public class Consumer
    {
        private static volatile Consumer instance;
        private static object syncRoot = new Object();
        private readonly monarchui.Configuration myconf;
        public event EventHandler<SignalEventArgs> msgEvent;
        private readonly BlockingCollection<String> queue;

        public static Consumer Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new Consumer();
                            Task.Factory.StartNew(() =>
                            {
                                instance.consume("");
                            }
                            );
                        }
                    }
                }

                return instance;
            }
        }


        private Consumer()
        {
            myconf = Configuration.Instance;
            queue = new BlockingCollection<String>(new ConcurrentQueue<String>(), 1000);
        }


        public void consume(string topic)
        {

            using (NetMQContext context = NetMQContext.Create())
            using (NetMQSocket subscriber = context.CreateSubscriberSocket())
            {
                subscriber.Connect(myconf.SubscribeEndpoint);
                subscriber.Subscribe(topic);
                subscriber.Options.Linger = new TimeSpan(0, 0, 0, 2); //  Seconds ;
                subscriber.Options.ReceiveHighWatermark = 100;
                subscriber.Options.ReceivevBuffer = 100;
                subscriber.Options.ReceiveTimeout = new TimeSpan(0, 0, 0, 1); //  Seconds

                subscriber.ReceiveReady += (sender, e) =>
                {
                    string address = subscriber.ReceiveString();
                    string contents = subscriber.ReceiveString();
                    //Console.WriteLine("{0} : {1}", address, contents);
                    queue.Add(contents);            
                };

                using (var poller = new Poller())
                {
                    poller.AddSocket(subscriber);
                    poller.Start();
                }
            }
        }

        public void init()
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    String value = queue.Take();

                    try
                    {
                        var h = JsonSerializer.DeserializeFromString<Signal>(value);
                        OnSignal(new SignalEventArgs { Sig = h });
                    }
                    catch (Exception err)
                    {
                        Console.WriteLine("navConsume: " + err.Message);
                    }

                }
            });
        }

        protected virtual void OnSignal(SignalEventArgs e)
        {
            EventHandler<SignalEventArgs> handler = msgEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        
        /*
        public static void Main(string[] f)
        {
            Consumer c = new Consumer();
            c.msgEvent += c_Reached;
            c.init();
            c.consume("SIG");
        }

        static void c_Reached(object sender, SignalEventArgs e)
        {
            Console.WriteLine(e.Sig.Symbol);
        }
         */ 
    }
}
