﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace monarchui
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        void App_Startup(object sender, StartupEventArgs e)
        {

            Trader tradeWindow = new Trader();
            tradeWindow.Top = 100;
            tradeWindow.Left = 400;
            tradeWindow.Show();
        }
    }
}
