﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace monarchui
{
    public class Signal : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private string symbol;
        private string bias;
        private double average;
        private double variance;
        private double stddev;
        private double midprice;

        public string Symbol
        {
            get { return symbol; }
            set
            {
                if (symbol != value)
                {
                    symbol = value;
                    OnPropertyChanged("Symbol");
                }
            }
        }

        public string Bias
        {
            get { return bias; }
            set
            {
                if (bias != value)
                {
                    bias = value;
                    OnPropertyChanged("Bias");
                }
            }
        }

        public double MidPrice
        {
            get { return midprice; }
            set
            {
                if (midprice != value)
                {
                    midprice = value;
                    OnPropertyChanged("MidPrice");
                }
            }
        }

        public double Average
        {
            get { return average; }
            set
            {
                if (average != value)
                {
                    average = value;
                    OnPropertyChanged("Average");
                }
            }
        }

        public double StdDev
        {
            get { return stddev; }
            set
            {
                if (stddev != value)
                {
                    stddev = value;
                    OnPropertyChanged("StdDev");
                }
            }
        }

        public double Variance
        {
            get { return variance; }
            set
            {
                if (variance != value)
                {
                    variance = value;
                    OnPropertyChanged("Variance");
                }
            }
        }

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
