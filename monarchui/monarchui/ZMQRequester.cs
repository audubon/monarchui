﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using NetMQ;
using ServiceStack.Text;

namespace monarchui
{
    public class ZMQRequester
    {
        private readonly monarchui.Configuration myconf;

        public ZMQRequester()
        {
            myconf = Configuration.Instance;
        }


        public void request(CommandTypes commandType, string symbol, string argument)
        {
            Dictionary<string, string> cmdMap = new Dictionary<string, string>
             {
                 { "ACTION", commandType.ToString() },
                 { "TARGET", symbol },                 
                 { "ARGUMENT", argument }
             };
            request(commandType, cmdMap);
        }


        public void request(CommandTypes commandType, string symbol)
        {
            Dictionary<string, string> cmdMap = new Dictionary<string, string>
             {
                 { "ACTION", commandType.ToString() },
                 { "TARGET", symbol }                                
             };

            request(commandType, cmdMap);
        }

        private void request(CommandTypes commandType, Dictionary<string, string> cmdMap)
        {
            string message = JsonSerializer.SerializeToString(cmdMap);

            using (NetMQContext context = NetMQContext.Create())
            using (NetMQSocket clientSocket = context.CreateRequestSocket())
            {
                clientSocket.Connect(myconf.ReqRepEndpoint);

                clientSocket.Options.ReceiveTimeout = new TimeSpan(0, 0, 0, 5);
                clientSocket.SendMore("TOPIC");
                clientSocket.Send(message);



                clientSocket.ReceiveReady += (sender, e) =>
                {
                    string replyjson = clientSocket.ReceiveString();
                    //Console.WriteLine(replyjson);                        
                };




                using (var poller = new Poller())
                {
                    poller.AddSocket(clientSocket);
                    poller.PollTimeout = 2;
                    poller.Start();
                }
            }
        }
    }
}
