﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace monarchui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static object _lock = new object();
        ObservableCollection<Signal> _SigCollection;

        public ObservableCollection<Signal> SigCollection
        {
            get { return _SigCollection; }
        }

        public void fillHandler(object sender, SignalEventArgs e)
        {
            for (int i = 0; i < _SigCollection.Count; i++)
            {
                if (_SigCollection[i].Symbol == e.Sig.Symbol)
                {
                    _SigCollection[i] = e.Sig;
                    return;
                }
            }

            _SigCollection.Add(e.Sig);
        }


        public MainWindow()
        {
            InitializeComponent();

            Consumer consumer = Consumer.Instance;
            consumer.init();
            consumer.msgEvent += this.fillHandler;
            _SigCollection = new ObservableCollection<Signal>();
            BindingOperations.EnableCollectionSynchronization(_SigCollection, _lock);
        }

        public override void OnApplyTemplate()
        {
            TextBlock footerTextBlock = this.GetTemplateChild("footerBlock") as TextBlock;
            footerTextBlock.Text = "Monarch Endpoint: " + Configuration.Instance.SubscribeEndpoint;
            base.OnApplyTemplate();
        }
    }
}
