﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ServiceStack.Text;

namespace monarchui
{
    /// <summary>
    /// Interaction logic for Trader.xaml
    /// </summary>
    public partial class Trader : Window
    {
        private readonly ZMQRequester zmqr;

        public Trader()
        {
            zmqr = new ZMQRequester();
            InitializeComponent();
        }


        private void attackPrice(object sender, RoutedEventArgs e)
        {
            var ticker = ((Button)sender).Tag;
            var actorName =  "tradeCoordActor/" + ticker;
            Task.Factory.StartNew(() =>
            {
                zmqr.request(CommandTypes.ATTACK, actorName);
            });
        }

        private void retreatPrice(object sender, RoutedEventArgs e)
        {

            var ticker = ((Button)sender).Tag;
            var actorName = "tradeCoordActor/" + ticker;

            Task.Factory.StartNew(() =>
            {
                zmqr.request(CommandTypes.RETREAT, actorName);
            });
        }

        private void disableActor(object sender, RoutedEventArgs e)
        {
            var ticker = ((Button)sender).Tag;
            var actorName = "tradeCoordActor/" + ticker;
            Task.Factory.StartNew(() =>
            {
                zmqr.request(CommandTypes.ETFDISABLE, actorName);
            });
        }

        private void onAttack(object sender, RoutedEventArgs e)
        {             
            var actorName = "tradeCoordActor";
            Task.Factory.StartNew(() =>
            {
                zmqr.request(CommandTypes.ATTACK, actorName);
            });
        }

        private void onRetreat(object sender, RoutedEventArgs e)
        {
            var actorName = "tradeCoordActor";
            Task.Factory.StartNew(() =>
            {
                zmqr.request(CommandTypes.RETREAT, actorName);
            });
        }

        private void onDisable(object sender, RoutedEventArgs e)
        {

            this.enableBidbtn.IsEnabled = true;
            this.enableAskbtn.IsEnabled = true;
            this.disablebtn.IsEnabled = false;

            var actorName = "tradeCoordActor";
            Task.Factory.StartNew(() =>
            {
                zmqr.request(CommandTypes.DISABLEALL, actorName);
            });
        }

        private void onEnableBid(object sender, RoutedEventArgs e)
        {
            this.enableBidbtn.IsEnabled = false;
            this.enableAskbtn.IsEnabled = false;
            this.disablebtn.IsEnabled = true;


            var actorName = "tradeCoordActor";
            Task.Factory.StartNew(() =>
            {
                zmqr.request(CommandTypes.ENABLE, actorName,"BUY");
            });
        }

        private void onEnableAsk(object sender, RoutedEventArgs e)
        {
            this.enableBidbtn.IsEnabled = false;
            this.enableAskbtn.IsEnabled = false;
            this.disablebtn.IsEnabled = true;

            var actorName = "tradeCoordActor";
            Task.Factory.StartNew(() =>
            {
                zmqr.request(CommandTypes.ENABLE, actorName, "SELL");
            });
        }
    }
}
